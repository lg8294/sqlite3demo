//
//  ViewController.m
//  Sqlite3_Demo
//
//  Created by apple on 15/6/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "ViewController.h"
#import "LGSqliteManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"%@", NSHomeDirectory());
    
    LGSqliteManager * db = [LGSqliteManager OpenDBWithName:MYDBNAME];
    
    NSArray * array = [db dataFromTable:TABLE_JUNIOR_ONE];
    NSArray * otherArray = [db dataFromTable:TABLE_JUNIOR_ONE grade:GradeJuniorOne unit:2];
    
    [LGSqliteManager deleteDBWIthName:MYDBNAME];
    
    [db close];
    
    array = [db dataFromTable:TABLE_JUNIOR_ONE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
