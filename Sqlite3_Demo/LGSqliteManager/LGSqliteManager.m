//
//  LGSqliteManager.m
//  Sqlite3_Demo
//
//  Created by apple on 15/6/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "LGSqliteManager.h"

@interface LGSqliteManager ()
{
    sqlite3 * _db;
}


@end

@implementation LGSqliteManager

- (void)dealloc
{
    _db = nil;
    NSLog(@"LGSqliteManager dealloc");
}

//打开数据库
- (instancetype)initWithName:(NSString *)dbName
{
    self = [super init];
    if (self) {
        
        //获取沙盒路径
        NSString * dbPath = [LGSqliteManager getPathWithDBName:dbName];
        
        //判断数据库是否存在
        NSFileManager * fileManager = [NSFileManager defaultManager];
        
        BOOL exist = [fileManager fileExistsAtPath:dbPath];
        
        if (!exist) {
            NSString * sourceDbPath = [[NSBundle mainBundle] pathForAuxiliaryExecutable:@"qisuwords.db"];
            
            if (sourceDbPath == nil) {
                NSLog(@"源数据库%@不存在!",SOURCE_DBNAME);
                return self;
            }
            else {
                
                NSError * error = nil;
                BOOL flag = [fileManager copyItemAtPath:sourceDbPath toPath:dbPath error:&error];
                
                if (!flag) {
                    
                    if (error) {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                    else {
                        NSLog(@"未知错误");
                    }
                    return self;
                }
                else {
                    NSLog(@"数据库%@创建成功",dbName);
                }
            }
        }
        
        if ([self openDBAtPath:dbPath]) {
            NSLog(@"数据库打开成功");
        }
        else {
            NSLog(@"数据库打开失败");
        }
    }
    return self;
}

//创建数据库
- (instancetype)initWithDbName:(NSString *)dbName data:(NSArray *)dataArray
{
    self = [super init];
    if (self) {
    }
    return self;
}

//打开数据库
+ (instancetype)OpenDBWithName:(NSString *)dbName
{
    return [[LGSqliteManager alloc] initWithName:dbName];
}

//创建数据库
+ (instancetype)creatDB:(NSString *)dbName Data:(NSArray *)dataArray
{
    return [[LGSqliteManager alloc] initWithDbName:dbName data:dataArray];
}

/**
 *  删除数据库
 *
 *  @param dbName 数据库名称
 *
 *  @return 是否成功
 */
+ (BOOL)deleteDBWIthName:(NSString *)dbName
{
    BOOL result = YES;
    
    NSString * dbPath = [LGSqliteManager getPathWithDBName:dbName];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSError * error = nil;
    
    if ([fileManager fileExistsAtPath:dbPath]) {
        
        result = [fileManager removeItemAtPath:dbPath error:&error];
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
    return result;
}


- (BOOL)openDBAtPath:(NSString *)daPath
{
    if (sqlite3_open(daPath.UTF8String, &_db) == SQLITE_OK) {
        return YES;
    }
    else {
        sqlite3_close(_db);
        return NO;
    }
}

//
- (NSArray *)dataFromTable:(NSString *)tableName
{
    NSString * sql = [NSString stringWithFormat:@"select * from %@", tableName];
    return [self readDataWithSql:sql];
}

- (NSArray *)dataFromTable:(NSString *)tableName grade:(GradeType)grade unit:(NSInteger)unit
{
    NSString * sql = [NSString stringWithFormat:@"select * from %@ where %@=%ld and %@=%ld", tableName, MY_WORD_GRADE, grade, MY_WORD_UNIT, unit];
//    NSString * sql = [NSString stringWithFormat:@"select * from %@ where %@='%ld'", tableName, MY_WORD_GRADE, grade];
    return [self readDataWithSql:sql];
}

- (NSArray *)readDataWithSql:(NSString *)sql
{
    NSMutableArray * result = [NSMutableArray array];
    
    if (_db == nil) {
        return result;
    }
    
    NSArray * keyTypeArray = @[
                               @{MY_WORD_ID:@(MY_WORD_ID_TYPE)},
                               @{MY_WORD_GRADE:@(MY_WORD_GRADE_TYPE)},
                               @{MY_WORD_MARK:@(MY_WORD_MARK_TYPE)},
                               @{MY_WORD_WORD:@(MY_WORD_WORD_TYPE)},
                               @{MY_WORD_PHONETIC:@(MY_WORD_PHONETIC_TYPE)},
                               @{MY_WORD_TRANSLATION:@(MY_WORD_TRANSLATION_TYPE)},
                               @{MY_WORD_EDITION:@(MY_WORD_EDITION_TYPE)},
                               @{MY_WORD_REMENBER:@(MY_WORD_REMENBER_TYPE)},
                               @{MY_WORD_UNIT:@(MY_WORD_UNIT_TYPE)}];
    
    //声明游标，sql指针
    sqlite3_stmt * stmt = nil;
    
    if (sqlite3_prepare_v2(_db, [sql UTF8String], -1, &stmt, nil) == SQLITE_OK) {
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            NSMutableDictionary *wordDic = [NSMutableDictionary dictionary];
            
            
            for (int i = 0; i < keyTypeArray.count; i++) {
                NSDictionary * keyDic = keyTypeArray[i];
                NSString * key = [[keyDic allKeys] firstObject];
                SqliteKeyType type = [[[keyDic allValues] firstObject] integerValue];
                
                switch (type) {
                    case SqlIntergerType:
                    case SqlIntType:
                    {
                        int temp = sqlite3_column_int(stmt, i);
                        [wordDic setObject:[NSString stringWithFormat:@"%d", temp] forKey:key];
                    }
                        break;
                    case SqlTextType:
                    {
                        char * temp = (char *)sqlite3_column_text(stmt, i);
                        if (temp != nil) {
                            NSString * str = [[NSString alloc] initWithUTF8String:temp];
                            [wordDic setObject:str forKey:key];
                        }
                        else {
                            [wordDic setObject:@"" forKey:key];
                        }
                    }
                        break;
                        
                    default:
                        break;
                }
            }
            
            [result addObject:wordDic];
        }
        sqlite3_finalize(stmt);
    }
    
    return result;
}

/**
 *  获取数据库路径（不论数据库是否存在）
 *
 *  @param dbName 数据库名称
 *
 *  @return 数据库路径
 */
+ (NSString *)getPathWithDBName:(NSString *)dbName
{
    //获取沙盒路径
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentPath = [paths objectAtIndex:0];
    
    NSString * dbPath = [documentPath stringByAppendingPathComponent:dbName];
    
    return dbPath;
}


/**
 *  创建一个独立的执行sql语句的方法，传入sql语句，就执行sql语句
 *
 *  @param sql 非查询的sql语句
 */
- (void)execSql:(NSString *)sql
{
    char *err;
    if (sqlite3_exec(_db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
        sqlite3_close(_db);
        NSLog(@"数据库操作数据失败!");
    }
}

/**
 *  关闭数据库
 */
- (void)close
{
    sqlite3_close(_db);
}
                               
@end

