//
//  LGSqliteManager.h
//  Sqlite3_Demo
//
//  Created by apple on 15/6/19.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

#define TABLE_JUNIOR_ONE @"com_example_yuanshangcao_qisuwords_entity_WordJunior1"
#define SOURCE_DBNAME @"qisuwords.db"

#define MYDBNAME @"allWords.sqlite"

#define MY_WORD_ID @"id"
#define MY_WORD_GRADE @"grade"
#define MY_WORD_MARK @"mark"
#define MY_WORD_WORD @"word"
#define MY_WORD_PHONETIC @"phonetic"
#define MY_WORD_TRANSLATION @"translation"
#define MY_WORD_EDITION @"edition"
#define MY_WORD_REMENBER @"remenber"
#define MY_WORD_UNIT @"unit"

#define MY_WORD_ID_TYPE             SqlIntergerType
#define MY_WORD_GRADE_TYPE          SqlIntergerType
#define MY_WORD_MARK_TYPE           SqlTextType
#define MY_WORD_WORD_TYPE           SqlTextType
#define MY_WORD_PHONETIC_TYPE       SqlTextType
#define MY_WORD_TRANSLATION_TYPE    SqlTextType
#define MY_WORD_EDITION_TYPE        SqlTextType
#define MY_WORD_REMENBER_TYPE       SqlTextType
#define MY_WORD_UNIT_TYPE           SqlIntType


typedef enum : NSUInteger {
    SqlIntergerType = 0,
    SqlTextType = 1,
    SqlIntType = 3,
    
}SqliteKeyType;

/**
 年级类型
 */
typedef enum : NSUInteger {
    GradeNone = 0,
    
    /**
     *  高一
     */
    GradeSeniorOne = 1,
    
    /**
     *  高二
     */
    GradeSeniorTwo = 2,
    
    /**
     *  高三
     */
    GradeSeniorThree = 3,
    
    /**
     *  初一
     */
    GradeJuniorOne = 7,
    
    /**
     *  初二
     */
    GradeJuniorTwo = 8,
    
    /**
     *  初三
     */
    GradeJuniorThree = 9,
    
    /**
     *  中考
     */
    GradeMedium = 4,
    
    /**
     *  高考
     */
    GradeCollege = 5,
    
} GradeType;

@interface LGSqliteManager : NSObject

/**
 *  打开指定名称的数据库
 *
 *  @param dbName 数据库名称
 *
 *  @return 包含数据库的对象
 */
+ (instancetype)OpenDBWithName:(NSString *)dbName;

/**
 *  通过传入的数据创建指定名称的数据库
 *
 *  @param dbName    数据库名称
 *  @param dataArray 传入的单词数据
 *
 *  @return 包含数据库的对象
 */
+ (instancetype)creatDB:(NSString *)dbName Data:(NSArray *)dataArray;

/**
 *  删除指定的数据库
 *
 *  @param dbName 数据库名
 *
 *  @return 是否删除成功
 */
+ (BOOL)deleteDBWIthName:(NSString *)dbName;

/**
 *  获取指定表名中的所有数据
 *
 *  @param tableName 表名
 *
 *  @return 表中的所有数据
 */
- (NSArray *)dataFromTable:(NSString *)tableName;

/**
 *  获取指定表名中指定年级和单元的所有数据
 *
 *  @param grade 年级
 *  @param unit  单元
 *
 *  @return 单词数据
 */
- (NSArray *)dataFromTable:(NSString *)tableName grade:(GradeType)grade unit:(NSInteger)unit;

- (void)close;

@end
